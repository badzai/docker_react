##### To start new docker react app: `bash tools/quick-start.sh -p <project_name> -o <port>`

- builds docker image using Dockerfile provided
- runs container of that image with name `-p <project_name>` on port `-o <port>`, default port is 3000

Inside container:

- installs node `FROM node`
- installs mongodb and runs `mongod --fork --logpath /var/log/mongodb/mongodb.log`
- runs `npx create-react-app project` inside container `/var/www`
- runs `npm start` from `/var/www/project`

Confirm installation by visiting http://localhost:[port]

##### To install bootstarp into your container: `docker exec -it <project_name> install-bootstrap`

##### To install  font-awesome into your container: `docker exec -it <project_name> install- font-awesome`

##### To destroy project: `bash tools/destroy.sh -p <project_name>`
