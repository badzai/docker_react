#!/bin/bash

set -e

if [[ -d /var/www/project ]]; then
    cd /var/www/project
    npm i font-awesome@4.7.0
    #yarn add font-awesome@4.7.0
fi

exec "$@"
