#!/bin/bash

set -e

if [[ ! -d /var/www/project ]]; then
    npx create-react-app project
fi

exec "$@"
