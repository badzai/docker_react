#!/bin/bash

set -e

mongod --fork --logpath /var/log/mongodb/mongodb.log

if [[ -d /var/www/project ]]; then
    cd /var/www/project
    npm start
fi

exec "$@"
