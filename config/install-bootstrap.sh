#!/bin/bash

set -e

if [[ -d /var/www/project ]]; then
    cd /var/www/project
    npm i bootstrap@4.1.1
fi

exec "$@"
