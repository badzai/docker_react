#!/bin/bash

set -e

while getopts p: option
do
        case "${option}"
        in
                p) PROJECT_NAME=${OPTARG};;
        esac
done

if [[ -d 'projects/'${PROJECT_NAME} ]]; then
    docker stop ${PROJECT_NAME} || echo 'Container not running. Moving on.'
    docker rm ${PROJECT_NAME} || echo 'Container not found. Moving on.'
    rm -rf 'projects/'${PROJECT_NAME}
fi
