#!/bin/bash

set -e

PORT=3000

if [[ ! -d 'projects' ]]; then
    mkdir projects
fi


while getopts p:o: option
do
        case "${option}"
        in
                p) PROJECT_NAME=${OPTARG};;
                o) PORT=${OPTARG};;
        esac
done

if [[ ! -d 'projects/'${PROJECT_NAME} ]]; then
    mkdir 'projects/'${PROJECT_NAME}
    docker build --build-arg DGID=$(id -g) --build-arg DUID=$(id -u) -t badzai/node .
    docker run -v $(pwd)/projects/${PROJECT_NAME}:/var/www -it -p ${PORT}:3000 -p 27017:27017 -p 3900:3900 --name ${PROJECT_NAME} badzai/node install
fi

docker start ${PROJECT_NAME} 