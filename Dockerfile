
FROM node:10.16.3

ARG DUID=0
ARG DGID=0

WORKDIR /var/www/

RUN apt-get -y update && \
    apt-get -y install mongodb && \
    mkdir -p /data/db

COPY config/install.sh /usr/local/bin/install
COPY config/install-bootstrap.sh /usr/local/bin/install-bootstrap
COPY config/install-font-awesome.sh /usr/local/bin/install-font-awesome
COPY config/entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh && \
    chmod +x /usr/local/bin/install && \
    chmod +x /usr/local/bin/install-bootstrap && \
    chmod +x /usr/local/bin/install-font-awesome

ENTRYPOINT ["/entrypoint.sh"]

RUN if [ "$DUID" != "0" ] && [ "$DGID" != "0" ] ; then \
    usermod -u ${DUID} www-data && \
    groupmod -g ${DGID} www-data; fi

RUN chown -R ${DUID}:${DGID} /data/db && \
    chown -R ${DUID}:${DGID} /var/log/mongodb

USER ${DUID}:${DGID}
